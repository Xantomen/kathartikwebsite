
<?php 

	include_once 'db_connect.php';
	include_once 'functions.php';
	
	sec_session_start();

	if (isset($_POST['email']) 
	&& isset($_POST['postal_code'])
	&& isset($_POST['country'])
	&& isset($_POST['city']))
	{
				
		//Fixes encoding issue that was happening only in the Hostgator server but not in localhost
		mysqli_set_charset($mysqli,"utf8");
				
		$email = $_POST['email'];
		$email = mysqli_real_escape_string($mysqli,$email);
		$postal_code = $_POST['postal_code']; 
		$postal_code = mysqli_real_escape_string($mysqli,$postal_code);
		$country = $_POST['country'];
		$country = mysqli_real_escape_string($mysqli,$country);
		$city = $_POST['city'];
		$city = mysqli_real_escape_string($mysqli,$city);
			
		
		if($email!="" && $postal_code!="" && $country!="" && $city!="")
		{		
						
			date_default_timezone_set('Europe/Berlin');
						
			$date_now = date("Y-m-d H:i:s");
			
			$sql="INSERT INTO `website_requests`(`email`, `postal_code`, `country`, `city`,`creation_date`) 
			VALUES ('value01','value02','value03','value04','value05')";
			
			$sql = str_replace("value01",$email,$sql);
			$sql = str_replace("value02",$postal_code,$sql);
			$sql = str_replace("value03",$country,$sql);	
			$sql = str_replace("value04",$city,$sql);
			$sql = str_replace("value05",$date_now,$sql);
			
			
			//Adding this replace to prevent fringe cases html code injection
			//It won't matter as long as I keep using .val() to get values.
			//Not using it for now, for cleaner code, less chasing arpund the special characters
			//$sql = str_replace("<","&lt",$sql); 
			
			$result = mysqli_query($mysqli,$sql);
			
			if($result == FALSE)
		    {
		        die('ERROR : ' . mysqli_error($mysqli));
	
		    }
			else {
				echo $result; 
			}
			
		}
		
  
	} 

	mysqli_close($mysqli);
				
?>
