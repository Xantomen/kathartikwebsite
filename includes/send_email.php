<?php

include_once 'db_connect.php';
include_once 'psl-config.php';

date_default_timezone_set('Etc/UTC');

require '../libs/PHPMailer/PHPMailerAutoload.php';


//Searching for questions whose resolutionDate is earlier than NOW (in epoch time)
//It will also check if the answer has been submitted already or not
//(as the reminder message is formed with the Prediction and What really happened)

$sql = "SELECT * FROM `website_requests` WHERE `is_email_sent`=FALSE";
//$sql = "SELECT * FROM questions WHERE `resolutionDate`<=1467295966 AND `isResolved`=FALSE AND `outcomeContent` IS NOT NULL";
//$sql = "SELECT * FROM questions WHERE `resolutionDate`<=1467295966 AND `outcomeContent` IS NOT NULL";

$result = mysqli_query($mysqli,$sql) or die(mysqli_error($mysqli)); 
$num_rows  = mysqli_num_rows($result);


             
if($num_rows > 0){
    // We have at least one match. For each, find all the users that need to receive an email about this.
   	//This should be, anyone that submitted an answer about the topic, and only their last guess if they
   	//entered more than one.
   			
   	//$data = array();
   		
	while ($row = mysqli_fetch_array($result)) {
						
		$id=$row["ID"];
	    $email= $row["email"];
		$postal_code = $row["postal_code"];
		$city= $row["city"];
		$country = $row["country"];
		$number_times_service_provided = $row["number_times_service_provided"];
			    
		$mail = new PHPMailer;
		$mail->isSMTP();
		$mail->SMTPDebug = 2;
		$mail->Debugoutput = 'html';
		$mail->Host = 'mail.xantomen.com';
		$mail->Port = 587;
		$mail->SMTPSecure = 'tls';
		$mail->SMTPAuth = true;
		
		$mail->Username = MAILUSER;
		$mail->Password = MAILPASSWORD;
		$mail->setFrom('egghatchersberlin@xantomen.com', 'PokeHatchers');
		$mail->addReplyTo('egghatchersberlin@xantomen.com', 'PokeHatchers');
		$mail->addAddress('egghatchersberlin@xantomen.com', '');
		$mail->Subject = 'PokeHatchers New request: '.$id.'-'.$email.'-'.$postal_code.'-'.$city.'-'.$country.'.';
		$mail->Body = '
		<br /> 
		New request from the website!<br />
		
		'.$id.'-'.$email.'-'.$postal_code.'-'.$city.'-'.$country.'.
		
		<br /> 
		
		Number times service has been provided to this person: '.$number_times_service_provided;
		
		

		$mail->AltBody = 'The request from the website.';
	    
		if (!$mail->send()) {
		    echo "Mailer Error: " . $mail->ErrorInfo;
		} else {
		    echo "SENT REQUEST EMAIL";
			
			$sql2="UPDATE `website_requests` SET `is_email_sent`=TRUE WHERE `ID`=".$id." AND `email`='".$email."'";
							
			$result2 = mysqli_query($mysqli,$sql2);
			
			if($result2 == FALSE)
		    {
		        die('Error : ' . mysql_error());
	
		    }
			
		}
			
	}
}else{
    
    echo 'NO QUESTIONS FOUND';
	
}


mysqli_close($mysqli);
exit();
    


