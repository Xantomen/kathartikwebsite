@function randomizeColor(){
  $color: (
    octal: (
      red: random(256)-1,
      green: random(256)-1,
      blue: random(256)-1
    ),
    percent: (
      red: random(101)-1,
      green: random(101)-1,
      blue: random(101)-1,
      saturation: random(101)-1,
      light: random(101)-1
    ),
    deg: (
      hue: random(361)-1
    ),
    fraction: (
      alpha: random(20)/100 + 0.4 //Makes them always be at least 0.4 visible
    )
  );
  @return $color;
}

@function color($base, $component) {
  $color: randomizeColor();
  @return map-get(map-get($color, $base), $component);
}

@function getRandomColor($format: NULL) {
  @if $format == "hex" {
    @return rgb(color(octal, red), color(octal, green), color(octal, blue));
  }
  @else if $format == "rgb" { 
    @return unquote("rgb(#{color(octal, red)}, #{color(octal, green)}, #{color(octal, blue)})");
  }
  @else if $format == "%rgb" { 
    @return unquote("rgb(#{color(percent, red)}%, #{color(percent, green)}%, #{color(percent, blue)}%)");
  }
  @else if $format == "rgba" { 
    @return unquote("rgba(#{color(octal, red)}, #{color(octal, green)}, #{color(octal, blue)}, #{color(fraction, alpha)})");
  }
  @else if $format == "%rgba" { 
    @return unquote("rgba(#{color(percent, red)}%, #{color(percent, green)}%, #{color(percent, blue)}%, #{color(fraction, alpha)})");
  }
  @else if $format == "hsl" { 
    @return unquote("hsl(#{color(deg, hue)}, #{color(percent, saturation)}%, #{color(percent, light)}%)");
  }
  @else if $format == "hsla" { 
    @return unquote("hsla(#{color(deg, hue)}, #{color(percent, saturation)}%, #{color(percent, light)}%, #{color(fraction, alpha)})");
  }
  @else { 
    @return nth($named-colors, random(147));
  }
}

@function shiftList($shift,$old_list){
  
  $random_color_list: ();

  $shift: $shift % (length($old_list) - 1);

  @for $i from 1 through length($old_list) {
       
       @if($i < $shift+1)
       {
         $random_color_list: append($random_color_list, nth($old_list, length($old_list) -$shift + $i), space);
       }
       /*@else if($i > length($random_color_list0) - $shift)
       {
         //$random_color_list: append($random_color_list, nth($old_list, $i), space);
         $random_color_list: append($random_color_list, nth($old_list, ($i + $shift) % length($random_color_list0)), space);
       }*/
       @else
       {
         //$random_color_list: append($random_color_list, nth($old_list, $i), space);
         $random_color_list: append($random_color_list, nth($old_list, $i - $shift), space);
       }
  }
  
  @return $random_color_list;
}

$random_color_list_original: ();

$random_color_lists: ();

/*Create original random color list*/
@for $i from 0 through 20 {
  $random_color_list_original: append($random_color_list_original, $i, space);
}

/*Create shifted lists for every row*/
@for $i from 0 through 20 {
  $new_random_color_list: shiftList($i,$random_color_list_original);
  
  $random_color_lists: append($random_color_lists, $new_random_color_list, comma);
}

@for $i from 1 through length(nth($random_color_lists, 1)) {
     
    .stuff-#{$i - 1} {
        color: nth(nth($random_color_lists, 1), $i);
    }
}
