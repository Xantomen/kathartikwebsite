'use strict';

var ENVIRONMENT = "DEVELOPMENT";



if(document.location.hostname != "localhost") ENVIRONMENT = "PRODUCTION";


/*var EmailInput = React.createClass({
	
	render:function() {
		
		return (
			
			<input type={this.props.input_type} className={this.props.input_validation} onChange={this.updateEmailValue} placeholder={this.props.input_placeholder}/>
		);
	},
  	updateEmailValue: function(event) {
  		
  		RequestData.emailResponder = event.target.value;
  	
  	}
	
});*/

var BootstrapButton = React.createClass({
  render: function() {
    return (
      <a {...this.props}
        href="javascript:;"
        role="button"
        className={(this.props.className || '') + ' btn'} />
    );
  }
});

var BootstrapModal = React.createClass({
	
	getInitialState: function() {
    // Start off by selecting the first board
	    return {
	      modalTitle: "SUCCESS!"
	    }
	  },
  // The following two methods are the only places we need to
  // integrate Bootstrap or jQuery with the components lifecycle methods.
  componentDidMount: function() {
    // When the component is added, turn it into a modal
    $(this.refs.root).modal({backdrop: 'static', keyboard: false, show: false});

    // Bootstrap's modal class exposes a few events for hooking into modal
    // functionality. Lets hook into one of them:
    $(this.refs.root).on('hidden.bs.modal', this.handleHidden);
  },
  componentWillUnmount: function() {
    $(this.refs.root).off('hidden.bs.modal', this.handleHidden);
  },
  close: function() {
    $(this.refs.root).modal('hide');
  },
  open: function() {
    $(this.refs.root).modal('show');
    
    this.setState({
      modalTitle: "SUCCESS!"
   	});
  },
  render: function() {
    var confirmButton = null;

    if (this.props.confirm) {
      confirmButton = (
        <BootstrapButton
          onClick={this.handleConfirm}
          className="btn-primary">
          {this.props.confirm}
        </BootstrapButton>
      );
    }
    
    return (
      <div className="modal fade" ref="root">
        <div className="modal-dialog">
          <div className="modal-content">
            <div className="modal-header">
              <button
                type="button"
                className="close"
                onClick={this.handleConfirm}>
                &times;
              </button>
              <h3>{this.state.modalTitle}</h3>
            </div>
            <div className="modal-body">
              {this.props.children}
            </div>
            <div className="modal-footer">
              {confirmButton}
            </div>
          </div>
        </div>
      </div>
    );
  },
  handleConfirm: function() {
    
      this.props.onConfirm();
      
  }
  
});

var Diamond = React.createClass({

	getInitialState: function() {
	    // Start off by selecting the first board
	    return {
	      positionalAnimation: "originalPosition",
	      index: this.props.diamondIndex
	    }
	  },

	render: function() {
			
		var siteContent = "noSiteContent";
			
		if(this.props.siteLink != "")
		{	
		 	siteContent = "hasSiteContent";
		}	
				
		var classes = "diamond "+this.state.positionalAnimation+" "+siteContent+" "+this.props.availabilityCell+" "+this.props.specialElements+" "+this.props.steppedOnStatus;
		var content_classes = "content "+this.state.positionalAnimation+" "+siteContent;
		var iframe_visual_classes = "visual_iframe "+this.state.positionalAnimation+" "+siteContent+" "+this.props.specialElements+" "+this.props.steppedOnStatus;
		var iframe_click_classes = "prevent_iframe_click "+this.state.positionalAnimation+" "+siteContent+" "+this.props.availabilityCell;
		
		var id = "diamond-index-"+this.props.diamondIndex;
		var iframe_id = "iframe-diamond-index-"+this.props.diamondIndex;		
				
			
		return (
			
			<div className={classes} id={id}>
				<div className={content_classes} onClick={this.activateContent}>
					{/*<div className="middle_text">Lorem ipsus lalalala whatdafucken christus</div>*/}
					<iframe id={iframe_id} className={iframe_visual_classes} src={this.props.siteLink}></iframe>
					<div className={iframe_click_classes} onClick={this.activateContent}></div>
				</div>
				
			</div>
	
		);
	},
	activateContent: function() {
  	  	
	  	var isAvailableCell = this.props.addCellToSteppedOn(this.state.index);
 	
	  	//e.stopPropagation();
	  	
	  	if(this.props.webpageMood == "game" && (isAvailableCell && this.state.index == this.props.lastSteppedOnCell))
	  	{
	  		
	  		if(this.state.positionalAnimation == "originalPosition")
		  	{
		  		
	  			this.setState({
			      positionalAnimation: "activateContent"
			   	});

		  	}
		  	else
		  	{
		  		this.setState({
			      positionalAnimation: "originalPosition"
			   });
		  	}
	  	}
	  	
	  	if(this.props.webpageMood == "visual" && isAvailableCell)
	  	{
	  		
	  		if(this.state.positionalAnimation == "originalPosition")
		  	{
		  		
	  			this.setState({
			      positionalAnimation: "activateContent"
			   	});

		  	}
		  	else
		  	{
		  		this.setState({
			      positionalAnimation: "originalPosition"
			   });
		  	}
	  	}
	  	
	  	
	 
  }
	
});
  

var KathartikApp = React.createClass({

  getInitialState: function() {
    // Start off by selecting the first board
              
    return {
      width:0,
      height:0,
      numColumns: 0,
      numRows: 0,
      mapState: 0,
      adjacentCells: [0],
      availableCells: [0],
      steppedOnCells: [],
      lastSteppedOnCell: -1,
      holdingSmallKeys: 0,
      holdingTreasureKey: 0,
      webpageMood: "visual"
      
    }
  },

  render: function() {
  	     	 
  	var header_content = "";
  	  	
  	var request_form = "";
  	  	
  	var modal = null;
  	
  	var diamond_grid = "";
  	
    
            
    modal = (
      <BootstrapModal
        ref="modal"
        confirm="OK"
        onConfirm={this.handleConfirm}
        title="Success!">
        	"I'll get back to you soon!"
        	<br/>
	
      </BootstrapModal>
    );
  	
  	var diamonds_list = [];
  	   	
  	
  	if(this.state.webpageMood == "game")
  	{
  		for(var i=0;i<MapObjects.site_links.length;i++)
	  	{
	  		var availability_cell = this.checkCellOnMatrixAvailability(i);
	  		
	  		//var availability_cell = "notAvailableCell";
  		
	  		diamonds_list.push(<Diamond key={i} diamondIndex={i} 
	  			siteLink={MapObjects.site_links[i]} specialElements={MapObjects.special_elements[i]} 
	  			availabilityCell={availability_cell} 
	  			steppedOnStatus={MapObjects.stepped_on_statuses[i]} addCellToSteppedOn={this.addCellToSteppedOn} lastSteppedOnCell={this.state.lastSteppedOnCell}
	  			webpageMood={this.state.webpageMood}/>);
	  	  	
	  	}	
  	}
  	if(this.state.webpageMood == "visual")
  	{
  		var row = 0;
  		var column = 0;
  		
  		for(var i=0;i<MapObjects.site_links.length;i++)
	  	{	  		
	  		
	  	  if(row % 2 == 0) {
            
             if(column == this.state.numColumns) {
            
		          row++;
		          column = 0;            
             }          
                        
          } else
          {
            if(column == this.state.numColumns - 1) {
            
              row++;
		      column = 0;  
              
            }             
           
          }
  		
	  		diamonds_list.push(<Diamond key={i} diamondIndex={i} 
	  			siteLink={MapObjects.site_links[i]} specialElements={"visualCell visualCell-random-color-change-per-row"+row}
	  			availabilityCell="availableCell" 
	  			steppedOnStatus={MapObjects.stepped_on_statuses_visual[i]} addCellToSteppedOn={this.addCellToSteppedOn} lastSteppedOnCell="0"
	  			webpageMood={this.state.webpageMood}/>);
	  	  	
	  	  	column++;
	  	}	
  	}
  	
  	var objectTaken1 = "objectNotTaken";
  	var objectTaken2 = "objectNotTaken";
  	var objectTaken3 = "objectNotTaken";
  	
  	if(this.state.holdingSmallKeys > 0)
  	{
  		objectTaken1 = "objectTaken";
  		
  		if(this.state.holdingSmallKeys > 1)
  		{
  			objectTaken2 = "objectTaken";
  		}
  	}
  	
  	if(this.state.holdingTreasureKey > 0)
  	{
  		objectTaken3 = "objectTaken";
  	}
  	  	   
  	var objectTaken1 = "smallKey_object "+objectTaken1;
  	var objectTaken2 = "smallKey_object "+objectTaken2;
  	var objectTaken3 = "treasureKey_object "+objectTaken3;
  	
  	var hud;  	   
  	
  	if(this.state.webpageMood == "game")
	  	{ 
	  	hud = (
	      <div id="playful_mode_hud">
		  		<div className="inventory_slot col-xs-4"><div className={objectTaken1}></div></div>
		  		<div className="inventory_slot col-xs-4"><div className={objectTaken2}></div></div>
		  		<div className="inventory_slot col-xs-4"><div className={objectTaken3}></div></div>
			</div>
	    );  	     
  	}  	  
  	  	    	  	   	     	    
    return (
				
		<div id="container_all" className="container-fluid">	
		
			<div id="preload_div">
	  			<div id="preload-01"></div>
	  			<div id="preload-02"></div>
	  			<div id="preload-03"></div>
	  			<div id="preload-04"></div>
	  			<div id="preload-05"></div>
	  			<div id="preload-06"></div>
	  			<div id="preload-07"></div>
	  			<div id="preload-08"></div>
	  		</div>
		  	
		  	
		  	{hud}
		  	
		  	<div id="diamonds">
		  		{diamonds_list}		
		  		
			</div>
			<button onClick={this.toggleWebpageMood}>Toggle Visual/Game Mode</button>
		  	
			{modal}
	
			
		</div>  		
		
    );
  },
  toggleWebpageMood: function(){
  	
  		if(this.state.webpageMood == "visual")
  		{
  		   this.setState({
		      webpageMood: "game",
		      steppedOnCells: [],
		      lastSteppedOnCell: -1
		   },function() {
		   	console.log(this.state.webpageMood);});
		   	
		    var objectTaken1 = "objectNotTaken";
		  	var objectTaken2 = "objectNotTaken";
		  	var objectTaken3 = "objectNotTaken";
  		}
  		else {
  			this.setState({
		      webpageMood: "visual",
		      steppedOnCells: [],
		      lastSteppedOnCell: -1
		   },function() {
		   	console.log(this.state.webpageMood);});
  		}
  		
	  		
  		
  },
  addCellToSteppedOn: function(index_value) {
  		  		
  	var steppedOnCells = this.state.steppedOnCells;
  	  	  	
  	var needAddIndex= true;
  	
  	var isAvailableCell = false;
  	
  	//Taking the startPoint class out of the first cell after one move
  	 
  	if(this.state.webpageMood == "game") 
  	{
	  	if(index_value == 0)
	  	{
	  		MapObjects.special_elements[0] = "normalCell";
	  		
	  	}
	  	
	  	  	
	  	if(this.checkCellOnMatrixAvailability(index_value).indexOf("availableCell") > -1)
		{		
			isAvailableCell = true;
			
			for(var k=0;k<steppedOnCells.length;k++)
			{
				if(index_value == steppedOnCells[k])
				{
					needAddIndex = false;
				}
			}
			
			if(needAddIndex)
			{
				MapObjects.stepped_on_statuses[index_value] = "steppedOn";
				steppedOnCells.push(index_value);
			}
		  	
		  	//console.log("1-->"+this.state.lastSteppedOnCell);
		  		  	
	  		this.setState({
		      steppedOnCells: steppedOnCells,
		      lastSteppedOnCell: index_value
		    },function() {
			 
			  switch(MapObjects.special_elements[index_value])
				{	
					case "smallKey":
					
						MapObjects.special_elements[index_value] = "normalCell";
								
						this.setState({
						      holdingSmallKeys: this.state.holdingSmallKeys+1,
						  
						    });
						 break;
					case "treasureKey":
					
					
						MapObjects.special_elements[index_value] = "normalCell";
						
						this.setState({
						      holdingTreasureKey: this.state.holdingTreasureKey+1,
						
						    });
					
						break;
					case "lockedDoor":
						
										
						if(this.state.holdingSmallKeys > 0)
						{
							MapObjects.special_elements[index_value] = "normalCell";
							
							this.setState({
						      holdingSmallKeys: this.state.holdingSmallKeys-1,
					
						    });
						}
						break;	
					case "treasureChest":
						
						if(this.state.holdingTreasureKey > 0)
						{
							MapObjects.special_elements[index_value] = "openedTreasure";
							
							this.setState({
						      holdingTreasureKey: this.state.holdingTreasureKey-1,
					
						    });
						}
					default:
						break;
					
				}	
			    
			    this.updateMatrixAvailability(this.state.numColumns);
			  
			  
			});
		    
		    //console.log("3-->"+this.state.lastSteppedOnCell);
		}    	    
	    
	} else if(this.state.webpageMood == "visual")
	{
		
		isAvailableCell = true;
		
		for(var k=0;k<steppedOnCells.length;k++)
		{
			if(index_value == steppedOnCells[k])
			{
				needAddIndex = false;
			}
		}
		
		if(needAddIndex)
		{
			MapObjects.stepped_on_statuses_visual[index_value] = "steppedOn";
			steppedOnCells.push(index_value);
		}
		
		this.setState({
	      steppedOnCells: steppedOnCells,
	      lastSteppedOnCell: index_value
	   	});
	}
	return isAvailableCell; 
  	
  	
  },
  checkCellOnMatrixAvailability: function(index_value) {
  	
  	var availability_cell = "notAvailableCell";
  	
  	var availableCells = this.state.availableCells;

	for(var k=0;k<availableCells.length;k++)
	{
		if(index_value == availableCells[k])
		{
			availability_cell = "availableCell";
		}
		if(index_value == this.state.lastSteppedOnCell)
		{
			availability_cell += " actualPositionCell";
		}
	}
  		  	
  	return availability_cell;
  	
  	
  },
  updateMatrixAvailability: function(numColumns) {
 
  	var availability_cell = "notAvailableCell";
  	
  	var steppedOnCells = this.state.steppedOnCells;
 
  	var availableCells = [];
  	
  	var adjacentCells = [];
  	
  	var adjacency_matrix = AdjacencyMatrices["AdjacencyMatrix"+numColumns];
  	   	  	
  	//Old, persistent way
  	   	  	
  	/*//For each stepped on Cell
  	for(var i=0;i<steppedOnCells.length;i++)
  	{
  		//For each adjacency value in the row corresponding to each stepped on cell
  		for(var j=0;j<adjacency_matrix[i].length;j++)
  		{  			
  			var index_value = j;
  			//If 1, adjacent, we add it to availableCells
  			if(adjacency_matrix[steppedOnCells[i]][j] == 1)
  			{
  				var alreadyInArray = false;
  				
  				for(var k=0;k<availableCells.length;k++)
  				{
  					if(index_value == availableCells[k])
  					{
  						alreadyInArray = true;
  					}
  				}
  				
  				if(!alreadyInArray)
  				{
  					  					
  					availableCells.push(index_value);
  				}
  			}
  			
  			//If 0, not adjacent, we don't do anything about it
  		}
  	}
  	
  	//Adding non-duplicates
  	for(var i=0;i<steppedOnCells.length;i++)
  	{
  		for(var j=0;j<availableCells;j++)
  		{
  			
	  		if(availableCells[j] == steppedOnCells[i])
			{
				availableCells.push(steppedOnCells[i]);
			}
		}
  		
  	}*/
  	
  	//For each stepped on Cell
  	  	
  	var last_stepped = this.state.lastSteppedOnCell;
  	
  	/*console.log(steppedOnCells);
  	console.log(last_stepped);*/
  	
  	if(last_stepped >= 0)
  	{
  		
  		//For each adjacency value in the row corresponding to each stepped on cell
		for(var j=0;j<adjacency_matrix[last_stepped].length;j++)
		{  			
			var index_value = j;
			//If 1, adjacent, we add it to adjacentCells
			
			if(adjacency_matrix[last_stepped][j] == 1)
			{
				
				var alreadyInArray = false;
			
				for(var k=0;k<adjacentCells.length;k++)
				{
					if(index_value == adjacentCells[k])
					{
						alreadyInArray = true;
					}
				}
				
				if(!alreadyInArray)
				{	
					var isCellAvailable = true;
										
					switch(MapObjects.special_elements[index_value])
					{	
						
						case "blockingWall":
						
							
							isCellAvailable = false;
						
							break;
						case "lockedDoor":
							
							if(this.state.holdingSmallKeys < 1)
							{
								isCellAvailable = false;
							}
							break;	
						case "treasureChest":
							
							if(this.state.holdingTreasureKey < 1)
							{
								isCellAvailable = false;
							}
						default:
						
							break;
						
					}	
					
					if(isCellAvailable)	
					{						
						availableCells.push(index_value);
					}
						
					adjacentCells.push(index_value);
				}
				
			}
			
			//If 0, not adjacent, we don't do anything about it
		}
	  	
	  	//Adding non-duplicates
	  	for(var i=0;i<steppedOnCells.length;i++)
	  	{
	  		for(var j=0;j<adjacentCells;j++)
	  		{
	  			
		  		if(adjacentCells[j] == steppedOnCells[i])
				{
					availableCells.push(steppedOnCells[i]);
					adjacentCells.push(steppedOnCells[i]);
				}
			}
	  		
	  	}
	  	
	  	adjacentCells.push(last_stepped);
	  	availableCells.push(last_stepped);
	  	
	  	this.setState({
	      adjacentCells: adjacentCells,
	      availableCells: availableCells
	    });
  	
  	}
  	
	
  },
  saveFormToDatabase: function(event) {
 
 	var email = $("#email").val();
 	var postal_code = $("#postal_code").val();
 	var country = "DE";
 	var city = "berlin";
 	
 	var validation = true;
 	
 	if(!validateEmail(email))
 	{
 		validation = false;
 		this.setState({
	      emailVerificationFailure: true
	    });
 	}
 	else
 	{
 		this.setState({
	      emailVerificationFailure: false
	    });
 	}
 	if(postal_code == "")
 	{
 		validation = false;
 		this.setState({
	      postalcodeVerificationFailure: true
	    });
 	}
 	else
 	{
 		this.setState({
	      postalcodeVerificationFailure: false
	    });
 	}
 	
 	if(validation)
 	{	
 		RequestData.email = email;
 		RequestData.postal_code = postal_code;
 		RequestData.country = country;
 		RequestData.city = city;
 		
 		saveFormToDatabase();
 		
 		this.refs.modal.open();
 		
   
 	}
 	
  },
  handleConfirm: function() {
    this.refs.modal.close();
  },updateDimensions: function() {

    var w = window,
        d = document,
        documentElement = d.documentElement,
        body = d.getElementsByTagName('body')[0],
        width = w.innerWidth || documentElement.clientWidth || body.clientWidth,
        height = w.innerHeight|| documentElement.clientHeight|| body.clientHeight;

		//Informing of number of rows and columns for resolution limits
		
		var numColumns = 0;
		var numRows = 0;
		var mapState = 0;
		
		if(width > 1416)
		{
			numColumns = 5;
			numRows = 4;
			mapState = 4;
			
		} else if(width > 1146) {
			
			numColumns = 4;
			numRows = 5;
			mapState = 3;
			
		} else if(width > 808) {
			
			numColumns = 3;
			numRows = 7;
			mapState = 2;
			
		} else {
			
			numColumns = 2;
			numRows = 12;
			mapState = 1;
		}
		
		
		//console.log("width"+width+",height"+height+",columns:"+numColumns+",rows"+numRows);

        this.setState({width: width, height: height, numColumns: numColumns, numRows: numRows, mapState: mapState });
        
        this.updateMatrixAvailability(numColumns); 
                
        /*
         * 
         * We are calling first row : Row0 and first column: Column0
         * Theres even rows (EVEN) and odd (ODD) rows
         * 
         * Cells pathfinding game:
         * 
         * Only allowed to move 1 cell at a time.
         * Only allowed to move diagonally.
         * This varies in different mappings.
         *
         * 
         * Map State 1: 18 cells, 2 columns, 12 rows
         * Map State 2: '' , 3 columns, 7 rows
         * Map State 3: '' , 5 columns, 4 rows
         * Map State 4: '' , 4 columns, 5 rows
         * 
         */       
       
       
    },
    componentWillMount: function() {
        this.updateDimensions();
    },
    componentDidMount: function() {
        window.addEventListener("resize", this.updateDimensions);
    },
    componentWillUnmount: function() {
        window.removeEventListener("resize", this.updateDimensions);
    }
  
});


var RequestData = {};

var NUM_CELLS_GRID = 18;

var MapColumns = {
	
	MapColumns2: makeMapColumns(NUM_CELLS_GRID,2),
	MapColumns3: makeMapColumns(NUM_CELLS_GRID,3),
	MapColumns4: makeMapColumns(NUM_CELLS_GRID,4),
	MapColumns5: makeMapColumns(NUM_CELLS_GRID,5)
};

var AdjacencyMatrices = {

	AdjacencyMatrix2: buildAdjacencyMatrix(NUM_CELLS_GRID,MapColumns.MapColumns2),
	AdjacencyMatrix3: buildAdjacencyMatrix(NUM_CELLS_GRID,MapColumns.MapColumns3),
	AdjacencyMatrix4: buildAdjacencyMatrix(NUM_CELLS_GRID,MapColumns.MapColumns4),
	AdjacencyMatrix5: buildAdjacencyMatrix(NUM_CELLS_GRID,MapColumns.MapColumns5),
};

var MapObjects = {
	
	site_links: [
  		"",
  		"",
  		"",
  		"",
  		"",
  		/*"",*/
  		"http://pokehatchers.com",
  		"",
  		"",
  		"",
  		"",
  		"",
  		"",
  		//"http://xantomen.com/gamesinputpal/#action=request_template&gameTitle=Boundaries%20of%20Light&controllerChosen=xbox360_controller",
  		"",
  		"",
  		"",
  		"",
  		"",
  		"",
  	], 
  	special_elements: [
  		"startPoint",
  		"normalCell",
  		"smallKey",
  		"normalCell",
  		"blockingWall",
  		"normalCell",
  		"blockingWall",
  		"lockedDoor",
  		"normalCell",
  		"lockedDoor",
  		"lockedDoor",
  		"blockingWall",
  		"treasureKey",
  		"normalCell",
  		"blockingWall",
  		"smallKey",
  		"blockingWall",
  		"treasureChest"
  		
  	],
  	stepped_on_statuses: [
  		"notSteppedOn",
  		"notSteppedOn",
  		"notSteppedOn",
  		"notSteppedOn",
  		"notSteppedOn",
  		"notSteppedOn",
  		"notSteppedOn",
  		"notSteppedOn",
  		"notSteppedOn",
  		"notSteppedOn",
  		"notSteppedOn",
  		"notSteppedOn",
  		"notSteppedOn",
  		"notSteppedOn",
  		"notSteppedOn",
  		"notSteppedOn",
  		"notSteppedOn",
  		"notSteppedOn"
    ],
    stepped_on_statuses_visual: [
  		"notSteppedOn",
  		"notSteppedOn",
  		"notSteppedOn",
  		"notSteppedOn",
  		"notSteppedOn",
  		"notSteppedOn",
  		"notSteppedOn",
  		"notSteppedOn",
  		"notSteppedOn",
  		"notSteppedOn",
  		"notSteppedOn",
  		"notSteppedOn",
  		"notSteppedOn",
  		"notSteppedOn",
  		"notSteppedOn",
  		"notSteppedOn",
  		"notSteppedOn",
  		"notSteppedOn"
    ]
  	
};

//Map when resolution at start is 1024
  	/*var special_elements = [
  		"startPoint",
  		"",
  		"smallKey",
  		"",
  		"blockingWall",
  		"",
  		"blockingWall",
  		"lockedDoor",
  		"",
  		"lockedDoor",
  		"lockedDoor",
  		"blockingWall",
  		"bigKey",
  		"",
  		"blockingWall",
  		"smallKey",
  		"blockingWall",
  		"treasureChest"
  	];*/
  	

initiateApp();

function restart_url() {
	window.location.hash = window.location.hash.split("#")[0];
}

/*function changeUrlToId(question_id) {
	window.location.hash = window.location.hash.split("#")[0]+"action=request_question&question_id="+question_id;
}*/

function initiateApp() {
		
	var action = getURLParameter("action");
	
	switch(action)
	{
		
		default:
														
			ReactDOM.render(<KathartikApp/>, document.getElementById('xantomen_react'));
		
			break;
		
			
	}
	
	if(ENVIRONMENT == "PRODUCTION")
	{
		/*mixpanel.track("web_started", {
	        "action_type": action
	    });	
	    
	    mixpanel.identify(mixpanel.get_distinct_id());
			
		mixpanel.people.set({
		    "$last_login": new Date()         // properties can be dates...
		});*/
	}
	
	
}



function makeMapColumns(totalCells,maxCellsRow)
{
	var MapColumns = [];
	var new_row = [];
	var column_index = 0;
	var row_index = 0;

	for(var i=0;i<totalCells;i++)
	{
		
		if(row_index%2 == 0)
		{
			
			if(column_index == maxCellsRow)
			{
				row_index++;
				column_index = 0;
				
				MapColumns.push(new_row);
				
				new_row = [];
				
				new_row.push(i);
			}
			else
			{
				new_row.push(i);
			}
		}
		else
		{
			if(column_index == maxCellsRow - 1)
			{
				row_index++;
				column_index = 0;
				
				MapColumns.push(new_row);
				
				new_row = [];
				
				new_row.push(i);
			}
			else
			{
				new_row.push(i);
			}
		}
		
		column_index++;
	
	}
	
	MapColumns.push(new_row);
	
	return MapColumns;
}

function buildAdjacencyMatrix(totalCells,MapColumnsArray)
{
	var AdjacencyMatrix = [];
			
	for(var i=0; i<totalCells; i++) {
		AdjacencyMatrix[i] = Array.apply(null, Array(totalCells)).map(Number.prototype.valueOf,0);
	}

	for(var i=0;i<MapColumnsArray.length;i++)
	{
		for(var j=0;j<MapColumnsArray[i].length;j++)
		{
			//If row is EVEN (index 0)
			//No need to ask for UNEVEN rows, as EVEN goes up and down when necessary
			if(i%2 == 0)
			{
				//If it's first row
				if(i==0)
				{
					if(j != 0)
					{	
						var index1 = MapColumnsArray[i][j];
						var index2 = MapColumnsArray[i+1][j-1];
					
						AdjacencyMatrix[index1][index2] = 1;
						AdjacencyMatrix[index2][index1] = 1;
												
						
					}
					if(j != MapColumnsArray[i].length - 1)
					{
						var index1 = MapColumnsArray[i][j];
						var index2 = MapColumnsArray[i+1][j];
					
						AdjacencyMatrix[index1][index2] = 1;
						AdjacencyMatrix[index2][index1] = 1;
						
					}
				}
				//If it's last row
				else if(i == MapColumnsArray.length - 1)
				{
					if(j != 0)
					{	
						var index1 = MapColumnsArray[i][j];
						var index2 = MapColumnsArray[i-1][j-1];
					
						AdjacencyMatrix[index1][index2] = 1;
						AdjacencyMatrix[index2][index1] = 1;
						
						
						
					}
					if(j != MapColumnsArray[i].length - 1)
					{
						var index1 = MapColumnsArray[i][j];
						var index2 = MapColumnsArray[i-1][j];
					
						AdjacencyMatrix[index1][index2] = 1;
						AdjacencyMatrix[index2][index1] = 1;
						
					}
				}
				//Any other even row
				else
				{
					if(j != 0)
					{	
						var index1 = MapColumnsArray[i][j];
						var index2 = MapColumnsArray[i+1][j-1];
					
						AdjacencyMatrix[index1][index2] = 1;
						AdjacencyMatrix[index2][index1] = 1;
						
						
						var index1 = MapColumnsArray[i][j];
						var index2 = MapColumnsArray[i-1][j-1];
					
						AdjacencyMatrix[index1][index2] = 1;
						AdjacencyMatrix[index2][index1] = 1;
						
						
						
					}
					if(j != MapColumnsArray[i].length - 1)
					{
						var index1 = MapColumnsArray[i][j];
						var index2 = MapColumnsArray[i+1][j];
					
						AdjacencyMatrix[index1][index2] = 1;
						AdjacencyMatrix[index2][index1] = 1;
						
						
						var index1 = MapColumnsArray[i][j];
						var index2 = MapColumnsArray[i-1][j];
					
						AdjacencyMatrix[index1][index2] = 1;
						AdjacencyMatrix[index2][index1] = 1;
						
					}
				}
			
			}
						
		}
	
	}
	
	return AdjacencyMatrix;

}

function saveFormToDatabase() {
		
	
}

function transformEpochTimestampToDate(epoch_timestamp) {
	
	var myDate = new Date(epoch_timestamp*1000);
	
	return myDate;
}

function validateEmail(email) {
  var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(email);
}

function getURLParameter(sParam)
{
	if(window.location.href.indexOf("#")>-1)
	{
		var sPageURL = window.location.href.split("#")[1];
    
    
    	var sURLVariables = sPageURL.split('&');
	    for (var i = 0; i < sURLVariables.length; i++) 
	    {
	        var sParameterName = sURLVariables[i].split('=');
	        if (sParameterName[0] == sParam) 
	        {
	            return sParameterName[1];
	        }
	    }
	}

}